alter table states
    drop constraint states_old_state_id_fkey,
    add foreign key (old_state_id) references states
        on delete set null;