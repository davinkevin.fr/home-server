#!/usr/bin/env sh

NOW=$(date -Iseconds -D "now")
END=$(date -Iseconds -d@"$(( `date +%s`+60*60*6))")

curl -qsLk 'https://alertmanager.monitoring.davinkevin.fr/api/v2/silences' \
  --json @- <<JSON
{
  "matchers":[
    {"name":"alertname","value":"LongRunningTransaction","isRegex":false,"isEqual":true},
    {"name":"job","value":"home-assistant/database-monitoring","isRegex":false,"isEqual":true},
    {"name":"namespace","value":"home-assistant","isRegex":false,"isEqual":true}
  ],
  "startsAt":"$NOW",
  "endsAt":"$END",
  "createdBy":"CronJob",
  "comment":"Disable notification for HASS+cnpg",
  "id":null
}
  JSON
