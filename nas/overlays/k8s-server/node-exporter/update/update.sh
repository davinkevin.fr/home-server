 #!/usr/bin/env bash

set -euo pipefail

cd $(mktemp -d)
scp -q fileserver.davinkevin.lan:/opt/node-exporter/version.env . && source version.env
CURRENT_VERSION=$VERSION

source /job-config/version.env
NEW_VERSION=$VERSION

if [ "$CURRENT_VERSION" = "$NEW_VERSION" ]; then
  echo "node-exporter version is up-to-date with version $CURRENT_VERSION"
  exit 0
fi

echo "New version has to be installed, old one is $CURRENT_VERSION and new is $NEW_VERSION"
scp -q /job-config/version.env fileserver.davinkevin.lan:/opt/node-exporter/
ssh fileserver.davinkevin.lan -t "bash install.sh"