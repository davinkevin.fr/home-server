#!/usr/bin/env sh

rclone --version

cd $(mktemp -d)
cp /.config/rclone/rclone.conf rclone.conf

rclone copy \
  --config rclone.conf \
  adguardhome:/opt/adguardhome/conf/AdGuardHome.yaml \
  v1.bucket.davinkevin.fr:backup/k8s/adguard/config/$(date +"%Y-%m-%dT%H-%M-%S")/