module.exports = {
    platform: 'gitlab',
    onboardingConfig: {
        extends: ['config:recommended'],
    },
    repositories: [
        'davinkevin.fr/home-server'
    ],
    allowedPostUpgradeCommands: ['^task renovate:update$'],
    hostRules: [{
        matchHost: "docker.io",
        username: process.env.DOCKERHUB_LOGIN,
        password: process.env.DOCKERHUB_TOKEN
    }]
};
