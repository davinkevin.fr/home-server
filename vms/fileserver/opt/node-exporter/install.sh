#!/usr/bin/env bash

set -euo pipefail

export ROOT_FOLDER=/opt/node-exporter
export VERSIONS_FOLDER=$ROOT_FOLDER/versions
cd $ROOT_FOLDER

source version.env
export VERSION=${VERSION/v/}
export OS=linux
export ARCH=amd64

echo "Download $VERSION"
mkdir -p $VERSIONS_FOLDER/$VERSION
cd $VERSIONS_FOLDER/$VERSION
curl -qsL https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.${OS}-${ARCH}.tar.gz |  \
    tar  --strip=1 -C . -xzvf - node_exporter-${VERSION}.${OS}-${ARCH}/node_exporter
chmod +x node_exporter

echo "Check binary is valid"
./node_exporter --version

echo "Stop previous node-exporter instance"
sudo systemctl stop node-exporter.service

echo "Link production binary"
ln -fs $VERSIONS_FOLDER/$VERSION/node_exporter $ROOT_FOLDER/node_exporter

echo "Start new production instance"
sudo systemctl start node-exporter.service