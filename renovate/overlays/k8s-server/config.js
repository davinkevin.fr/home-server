module.exports = {
    platform: 'gitlab',
    onboardingConfig: {
        extends: ['config:recommended'],
    },
    repositories: [
        'davinkevin.fr/home-server',
        'davinkevin.fr/projects/prezctl',
        'davinkevin.fr/projects/renovate/davinkevin-fr-renovate',
        "davinkevin.fr/projects/dotmakeup/bird.makeup",
        "davinkevin.fr/projects/dotmakeup/social-sidecar",
        'davinkevin.fr/projects/linkpage',
        "davinkevin.fr/presentations/istio-kubernetes-best-friend",
        'davinkevin/Podcast-Server',
    ],
    hostRules: [{
        matchHost: "docker.io",
        username: process.env.DOCKERHUB_LOGIN,
        password: process.env.DOCKERHUB_TOKEN
    }],
    allowedPostUpgradeCommands: ['^task renovate:update$']
};
