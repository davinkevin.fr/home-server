#!/usr/bin/env sh

cd $(mktemp -d)
cp /certs/tls* .
rclone copyto --config /keys/rclone.conf . adguardhome:/opt/adguardhome/certs --progress
